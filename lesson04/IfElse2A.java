package lesson04;

import java.util.Scanner;

import static libs.Console.print;

public class IfElse2A {
    public static String doRepresent(int x) {
        return (x < 10) ?
                String.format("%d is less than ten", x) :
                String.format("%d is greater or equal than ten", x);
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        String message = doRepresent(x);
        print(message);

    }
}
