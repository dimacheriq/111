package lesson04;

import java.util.Scanner;

import static libs.Console.print;

public class IfElse2 {
    public static String doProcess(int x) {
        String message;
        if (x < 10) {
            message = String.format("%d is less than ten", x);
        }else {
            message = String.format("%d is greater or equal than ten", x);
        } return message;
        }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        String message = doProcess(x);
        print(message);

    }
}
