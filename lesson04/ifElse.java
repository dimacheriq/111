package lesson04;

import java.io.InputStream;
import java.util.Scanner;

import static libs.StringUtils.*;

public class ifElse {


    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String line = s.nextLine();
        String message;
        if (isInt(line)) {message = "Integer was entered";}
        else if (isLong(line)) {message = "Long was entered";}
        else if (isDouble(line)){message = "Double was entered";}
        else {message = "Non-integer (String) was entered";}
        System.out.println(message);
        System.out.println("going further");
    }


}