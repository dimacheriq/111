package lesson04;

import java.util.Scanner;

import static libs.Console.print;

public class IfElse1 {

    public static void main(String[] args) {
       print("Enter the number:");
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        String message = switch (x) {
            case 1 -> "one";
            case 2,3 -> "three";
            case 11 -> "eleven";
            default -> String.format("else: %d\n", x);
        };print(message);
    }
}
