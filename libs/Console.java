package libs;

/**
 * System.out.print..........- JUST FORBIDDEN
 */
public class Console {
    public static void print(String line){
        System.out.println(line);
    }

    public static void main(String[] args) {
        print("Whatever");
        print(String.format("%d + %d = %d\n", 1, 1, 1 + 1));
        ScannerConstrained sc = new ScannerConstrained();
        String line = sc.nextLine();
        System.out.println(line + " It's Ok");
    }
}
