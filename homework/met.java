package homework;

public class met {
    static {
        System.out.println("Class loading...");
    }
    {
        System.out.println("Instance created");
    }
    static int sum2(int[] ints) {
        int sum = 0;
        int idx = 0;
        while (true) {
            if (idx == ints.length) break;
            sum = sum + ints[idx];
            idx = idx + 1;
            idx++;
        }
        return sum;
    }
}
