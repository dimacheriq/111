package homework;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;



public class HardHomework01 {

    public static void print(String line) {
        System.out.println(line);
    }




    public static void main(String[] args) {
        Random random = new Random();
        IntStream intStream1 = random.ints(0, 100);
        IntStream intStream2 = intStream1.limit(1);
        int r = intStream2.sum();
        //     print(String.valueOf(r));
        String s1 = "Hello";
        String s2 = "Enter your name:";
        print(String.format("%s!", s1));
        print(String.format("%s", s2));
        Scanner scanner = new Scanner(System.in);
        var name = scanner.nextLine();
        print(String.format("%s, %s!\nA number from 0 to 100, can you guess?\nLet the game begin!\nPlease, enter some number from 0 to 100: ", s1, name));




        while (true) {
            Scanner s = new Scanner(System.in);
            String line = s.nextLine();
            int[] all = line.chars().toArray();


            Arrays.sort(all);

            PrintStream err = System.err;
            try {
                int i = Integer.parseInt(line);

                if (r == i) {
                    print(String.format("Congratulation %s!\nThe random number war really %d", name, i));
                    print(String.format("Your numbers %s", Arrays.toString(all)));

                    break;
                }
                if (i < r) print("Your number is too small. Please try again.");
                if (i > r) print("Your number is too big. Please try again.");


                if (i < 0)
                    err.printf("Your number %s isn't correct.\n Please enter the number from 0 to 100:", line);
            } catch (NumberFormatException ex) {
                err.printf("Number %s wasn't successfully parsed\n", line);
                err.printf("Exception was: %s\n", ex.getMessage());
                print("Please enter the number from 0 to 100:");
            }
        }
    }
    }



