package lesson02;

import java.util.Arrays;
import java.util.Scanner;

public class TaskRand {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter first num and length");
        int initial = scan.nextInt();
        int size = scan.nextInt();
        double result = Math.floor(Math.random() * size) + initial;
        System.out.printf("Your result is %s", result);
    }
}
