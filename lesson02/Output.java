package lesson02;

import java.io.PrintStream;

public class Output {
    public static void main(String[] args) {
        PrintStream out = System.out;
        PrintStream err = System.err;
        out.println("Hello!");
        err.println("Something went wrong");
    }
}
