package lesson02;

import java.io.InputStream;
import java.util.Scanner;

public class input3 {
    public void DoSomething(String input) {
        //....
    }
    public static void main(String[] args) {
        InputStream in = System.in;
        var scanner = new Scanner(in);
        System.out.println("Enter your number :");
        String line = scanner.nextLine();
        try {
            int i = Integer.parseInt(line);
            System.out.printf("Number %d was successfully parsed", i);
        } catch (NumberFormatException ex) {
            System.out.printf("Number %s wasn't successfully parsed\n", line);
            System.out.printf("Exception was: %s\n", ex.getMessage());
        }
    }
}
