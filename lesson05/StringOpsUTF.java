package lesson05;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class StringOpsUTF {
    public static void main(String[] args) {
        String s0 = "дмитро";
        String s1 = Character.toUpperCase(s0.charAt(0))+s0.substring(1);
        System.out.println(s1);

        byte [] bytes = s0.getBytes(StandardCharsets.UTF_8);
        System.out.println(bytes.length);
        char [] chars = new char[s0.length()];
        s0.getChars(0,6,chars, 0);

        chars[0] =Character.toUpperCase(chars[0]);
        String s2 = new String(chars);
        System.out.println(s2);

        int[] ints = s0.chars().toArray();
        System.out.println(Arrays.toString(ints));

        char[] chars1 = s0.toCharArray();
        System.out.println(Arrays.toString(chars1));

        String contains = String.valueOf(s0.contains("1"));
        System.out.println(contains);

        String repeat = s0.repeat(7);
        System.out.println(repeat);
    }

}
