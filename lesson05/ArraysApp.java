package lesson05;
import libs.Console;

import java.util.Arrays;

public class ArraysApp {
    public static void iterationWay1(int[] data) {
                for (int x : data) {
            System.out.printf("%d ", x);
        }
        System.out.println();
    }

    // allocate memory;
    // Doesn't take original data;
    public static int [] reverse(int[]data){
        int[] outcome = new int[data.length];
        for (int i = 0; i < data.length; i++) {
              outcome[i] = data[data.length - i - 1];
        }
        return outcome;

    }

    // Doesn't allocate memory;
    // Destroys original data!!!
    // faster;
    public static void reverseInPlace(int []data) {
        for (int i = 0; i < data.length/2; i++) {
            int t = data[data.length - i - 1];
            data[data.length - i - 1] = data[i];
            data[i] = t;
            
        }
    }
    public static void main(String[] args) {
        // create:
        int[] ints1     = new int[10];
        int[] ints2     = {1, 2, 7, 5, 0};
        // size/length:
        int size1 = ints1.length;  //10
        int size2 = ints2.length;  //5
        iterationWay1(ints1);
        iterationWay1(ints2);
        ints1[0] = 0;
        ints1[1] = 2;
        ints1[9] = 7;
        iterationWay1(ints1);
        Arrays.sort(ints2);
        System.out.println(Arrays.toString(ints2));
        int [] reversed = reverse(ints2);
        System.out.println(Arrays.toString(reversed));
        reverseInPlace(reversed);
        System.out.println(Arrays.toString(reversed));
    }
}
