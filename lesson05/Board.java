package lesson05;

public class Board {
    public static String representCell(int value) {
        if (value == 1) return "@@";
        if (value == 3) return "00";
        if (value == 2) return "XX";
        return "..";
    }
    public static void printBoard(int [][]board){
        for (int[] ints : board) {
            for (int anInt : ints) {
                System.out.print("| " + representCell(anInt) +" ");
            }
            System.out.println();
            System.out.println("|____|____|____|____|____|____|____|____|____|____|");
        }

    }
    public static void fillBoard(int[][] board) {
        board[7][1] = 1;
        board[8][1] = 1;
        board[9][1] = 1;
        board[0][0] = 3;
        board[0][1] = 3;
        board[5][8] = 2;
    }
    public static void main(String[] args) {
        int [][] board = new int[10][10];
        fillBoard(board);
        printBoard(board);
    }
}
