package lesson05;

public class Strings1 {
    public static void main(String[] args) {
        char [] data = {65,65,65,'1','o','!' };
        String s0 = new String(data);
        System.out.println(s0);
        String line1 = "Hello";
        String line2 = "Viktoria";
        String line = String.format("%s ,%s!", line1, line2);
        System.out.println(line);
        String line0a = """
                Hello
                world""";
        System.out.println(line0a);

        String b = "1" +
                "www" +
                line1;
        System.out.println(b);

    }
}
