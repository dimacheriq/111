package lesson05;

import java.util.Arrays;

public class ArraysCopy {
    public static void main(String[] args) {
        int[] ints1 =  {1, 2, 7, 5, 0};
        System.out.println(Arrays.toString(ints1));
        int[] ints2 = new int[10];
        System.arraycopy(ints1, 2, ints2, 3, 2);
        System.out.println(Arrays.toString(ints2));
        int[] ints3 = Arrays.copyOf(ints1, 3);
        System.out.println(Arrays.toString(ints3));
        int[] ints10 = Arrays.copyOf(ints1, 10);
        System.out.println(Arrays.toString(ints10));
    }
}
