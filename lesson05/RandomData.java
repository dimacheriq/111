package lesson05;

import java.util.Arrays;
import java.util.Random;

public class RandomData {
    public static void main(String[] args) {
        int [] randoms = new Random()
                .ints(10,50)
                .limit(1)
                .distinct()
//                .sorted()
                .toArray();
        System.out.println(Arrays.toString(randoms));

    }
}
