package lesson03;

// import org.jetbrains.annotations.Contract;

import java.util.Arrays;

// import static sun.swing.MenuItemLayoutHelper.max;

public class ArraysApp {
    public static void main(String[] args) {
        int[] a = new int[10];
        var b = new double[10];
        char[] c = new char[5];
        char c0 = c[0]; // 0
        int i0 = c0 + 50;     // 50 (ASCII Table)
        char c50 = (char) i0; // 2 (ASCII Table)
        int i1 = 0 + '2';     // 50
        System.out.println((char) (c[0] + 50));

        String s2 = "0110abc";
        char[] chars2 = s2.toCharArray(); // ['0', '1', '1', '0', 'a', 'b', 'c']


        String s = "Дмитро";
        byte[] bytes = s.getBytes();
        System.out.println(bytes.length);

        String s3 = "Dmytro";
        byte[] bytes1 = s3.getBytes();
        System.out.println(bytes1.length);
        for (byte value : bytes1) {
            System.out.printf("%d ", value);
        }
        String s1 = "0110";
        char[] chars1 = s1.toCharArray(); // ['0', '1', '1', '0']
        int[] ints = new int[chars1.length];
        for (int i = 0; i < chars1.length; i++) {
            char cc1 = chars1[0]; // '0' - 48, '1' - 49
            //        48  - 48 => 0
            //        49  - 48 => 1
           int cc2 = cc1 - 48;
            int cc3 = cc1 - '0';
            ints[i] = cc3;
        }                                 // [0, 1, 1, 0]

        isNumber('4'); // true
        isNumber('0'); // true
        isNumber('a'); // false
//        isCharNumber(0);  // false
//
    }

    static boolean isNumber(char c) {
        return c >= '0' && c <= '9';
    }
    static int sum8(int[] ints) {
        return Arrays.stream(ints).sum();
    }
    static int sum(int[] ints) {
        int sum = 0;
        for (int i = 0; i < ints.length; i++) {
            sum = sum + ints[i];
        }
        return sum;
    }


    static int sum2(int[] ints) {
                int sum = 0;
                int idx = 0;
                while (true) {
                    if (idx == ints.length) break;
            sum = sum + ints[idx];
            idx = idx + 1;
            idx++;
        }
        return sum;
    }
    static int sum3(int[] ints) {
        int sum = 0;
        for (int anInt : ints) {
            sum = sum + anInt;
        }
        return sum;
    }

    public static void main1(String[] args) {
        int[] a = {1,2,30,5};
       // int m1 = max(a); // 30
        int[] b = {};
       // int m2 = max(b); // 30
        int[] c = {Integer.MIN_VALUE, Integer.MAX_VALUE, 0};
       // int m3 = max(c); // 30
    }

}